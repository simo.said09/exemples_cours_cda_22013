package fr.afpa.exemples.cours.constructeurs;

public class Voiture {

	public String marque;
	public String modele;
	public int vitesse;
	public int kilometrage = 300;
	public boolean estDemarree;

	public Voiture() {
		System.out.println("je suis dans le constructeur");
	}

	public Voiture(Voiture v) {
		this.vitesse = v.vitesse;
		this.marque = new String(v.marque);
		this.modele = new String(v.modele);
		this.kilometrage = v.kilometrage;
		this.estDemarree = v.estDemarree;
	}

	public Voiture clone() {
		Voiture newVoiture = new Voiture();
		newVoiture.vitesse = this.vitesse;
		newVoiture.marque = new String(this.marque);
		newVoiture.modele = new String(this.modele);
		newVoiture.kilometrage = this.kilometrage;
		newVoiture.estDemarree = this.estDemarree;
		return newVoiture;
	}

//	public static Voiture newInstance(Voiture v) {
//		Voiture newVoiture = new Voiture();
//
//		newVoiture.vitesse = v.vitesse;
//		newVoiture.marque = new String(v.marque);
//		newVoiture.modele = new String(v.modele);
//		newVoiture.kilometrage = v.kilometrage;
//		newVoiture.estDemarree = v.estDemarree;
//
//		return newVoiture;
//	}

	public Voiture(String marque, String modele) {
		this.marque = marque;
		this.modele = modele;

	}

	public Voiture(String marque, String modele, int vitesse, int kilometrage, boolean estDemarree) {
		this.marque = marque;
		this.modele = modele;
		this.vitesse = vitesse;
		this.kilometrage = kilometrage;
		this.estDemarree = estDemarree;
	}

	public boolean start() {
		this.estDemarree = !this.estDemarree;
		return this.estDemarree;
	}

	public int accelerer(int vitesse) {
		if (this.estDemarree)
			this.vitesse += vitesse;
		return this.vitesse;
	}

}
