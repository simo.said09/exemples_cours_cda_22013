package fr.afpa.gestion_banque.config;

import java.nio.file.Path;
import java.util.Map;

import fr.afpa.gestion_banque.services.FileConfigService;
import fr.afpa.momo_lib.utils.NumberUtils;

public class ConfigApp {
	public static final String SEPARATOR_CSV = ";";
	public static final String SEPARATOR_PROPERTIES = "=";
	public static final Path BASE_BDD_DIR = Path.of("C:/gestion_banque/BDD");
	public static boolean generateStub;
	
	public static Map<String,String> properties;

	static {
		try {
			properties = FileConfigService.getInstance().readAll();
			generateStub = NumberUtils.parseBoolean(properties.get("config.app.init_data"));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public ConfigApp() {

	}
}
