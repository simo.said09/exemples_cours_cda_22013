package fr.afpa.gestion_banque.controller;

import java.io.IOException;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;

public class AppController {

	private static AppController instance;

	private ClientDAO clientDAO;
	private AgenceDAO agenceDAO;
	private CompteBancaireDAO compteBancaireDAO;

	private AppController() {
		clientDAO = ClientDAO.getInstance();
		agenceDAO = AgenceDAO.getInstance();
		compteBancaireDAO = CompteBancaireDAO.getInstance();
	}

	public static AppController getInstance() {
		if (instance == null)
			instance = new AppController();
		return instance;
	}

	public void initData() {
		if(ConfigApp.generateStub) {
			try {
				clientDAO.initData();
				agenceDAO.initData();
				compteBancaireDAO.initData();
			} catch (IOException e) {
				throw new  IllegalArgumentException("TO DO");
			}
		
		}
	}

}
