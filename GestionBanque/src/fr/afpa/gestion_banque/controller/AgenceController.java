package fr.afpa.gestion_banque.controller;

import java.util.List;

import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.gestion_banque.view.AgenceView;
import fr.afpa.gestion_banque.view.AppView;

public class AgenceController {

	private static AgenceController instance = null;

	private AgenceView agenceView;
	private AgenceDAO agenceDAO;
	private AppView appView;
	private CompteBancaireDAO compteBancaireDAO;

	private AgenceController() {

		this.agenceView = AgenceView.getInstance();
		this.agenceDAO = AgenceDAO.getInstance();
		this.appView = AppView.getInstance();
		this.compteBancaireDAO = CompteBancaireDAO.getInstance();
	}

	public static AgenceController getInstance() {

		if (instance == null)
			instance = new AgenceController();

		return instance;
	}

	public void creation() {
		Agence newAgence = agenceView.affichageCreation();
		newAgence = agenceDAO.save(newAgence);
		appView.affichageMessage("L'agence " + newAgence.getCode() + " est cr��e");
	}

	public void affichageAll() {
		List<Agence> agences = agenceDAO.findAll();
		agenceView.affichageToutes(agences);
	}

	public void suppression() {
		int codeAgence = agenceView.affichageSuppression();
		if (0 == codeAgence) {
			appView.affichageMessage("impossible de supprimer l'agence par defaut");
			return;
		}
		if (!agenceDAO.exists(codeAgence)) {
			appView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}

		List<CompteBancaire> comptesBancaires = compteBancaireDAO.findAllByCodeAgence(codeAgence);

		for (CompteBancaire currentCB : comptesBancaires) {
			currentCB.getAgence().setCode(0);
			compteBancaireDAO.update(currentCB);
		}

		agenceDAO.deleteByCode(codeAgence);

		appView.affichageMessage("L'agence " + codeAgence + " est supprimee");
	}

	public void modification() {
		int codeAgence = agenceView.recuperationCodeAgence();
		Agence oldAgence = agenceDAO.findByCode(codeAgence);
		if (oldAgence == null) {
			appView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}
		Agence newAgence = agenceView.affichageModification(oldAgence);
		agenceDAO.update(newAgence);
		appView.affichageMessage("L'agence " + newAgence.getCode() + " est modifi�e");

	}
}
