package fr.afpa.gestion_banque.controller;

import java.util.List;

import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.view.CompteBancaireView;
import fr.afpa.gestion_banque.view.validator.CompteBancairesValidator;
import fr.afpa.momo_lib.utils.MyList;

public class CompteBancaireController {

	private static CompteBancaireController instance = null;

	private CompteBancaireDAO compteBancaireDAO;
	private AppView appView;
	private CompteBancaireView compteBancaireView;
	private CompteBancairesValidator compteBancairesValidator;

	private CompteBancaireController() {
		compteBancaireDAO = CompteBancaireDAO.getInstance();
		appView = AppView.getInstance();
		compteBancaireView = CompteBancaireView.getInstance();
		compteBancairesValidator = CompteBancairesValidator.getInstance();
	}

	public static CompteBancaireController getInstance() {

		if (instance == null)
			instance = new CompteBancaireController();

		return instance;
	}

	public void creation() {
		CompteBancaire newCompteBancaire = this.compteBancaireView.affichageCreation();

		boolean isOk = compteBancairesValidator.verificationRGs(newCompteBancaire);

		if (!isOk) {
			return;
		}
		newCompteBancaire = this.compteBancaireDAO.save(newCompteBancaire);

		this.appView.affichageMessage("Le compteBancaire " + newCompteBancaire.getId() + " est cr��");
	}

	public void affichageAll() {
		List<CompteBancaire> compteBancaires = this.compteBancaireDAO.findAll(false);
		this.compteBancaireView.affichageTous(compteBancaires);
	}

	public void suppression() {
		int idCompteBancaire = this.compteBancaireView.affichageSuppression();

		if (!this.compteBancaireDAO.exists(idCompteBancaire)) {
			this.appView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		this.compteBancaireDAO.deleteById(idCompteBancaire);
		this.appView.affichageMessage("Le compteBancaire " + idCompteBancaire + " est supprime");
	}

	public void modification() {
		int idCompteBancaire = this.compteBancaireView.recuperationIdCompteBancaire("Modification CompteBancaire : ");
		CompteBancaire oldCompteBancaire = this.compteBancaireDAO.findById(idCompteBancaire);
		if (oldCompteBancaire == null) {
			this.appView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		CompteBancaire newCompteBancaire = this.compteBancaireView.affichageModification(oldCompteBancaire);
		this.compteBancaireDAO.update(newCompteBancaire);
		this.appView.affichageMessage("Le compteBancaire " + newCompteBancaire.getId() + " est modifie");

	}

	public void chercherUnCompteBancaire() {
		int idCompteBancaire = this.compteBancaireView
				.recuperationIdCompteBancaire("Rechercher un compteBancaire par numero: ");
		CompteBancaire currentCompteBancaire = this.compteBancaireDAO.findById(idCompteBancaire,false);
		if (currentCompteBancaire == null) {
			this.appView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		List<CompteBancaire> compteBancaires = new MyList<>();
		compteBancaires.add(currentCompteBancaire);
		this.compteBancaireView.affichageTous(compteBancaires);

	}

	public void chercherTousCompteBancaireClient() {
		String idClient = this.compteBancaireView.recuperationIdClient("Afficher la liste des comptes d�un client : ");
		List<CompteBancaire> compteBancaires = this.compteBancaireDAO.findAllByIdClient(idClient);
		if (compteBancaires == null || compteBancaires.size() == 0) {
			this.appView.affichageMessage("Le client " + idClient + " ne possede pas de comptes bancaires");
			return;
		}
		this.compteBancaireView.affichageTous(compteBancaires);

	}
}
