package fr.afpa.gestion_banque.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.view.ClientView;
import fr.afpa.gestion_banque.view.CompteBancaireView;
import fr.afpa.momo_lib.utils.MyList;
import fr.afpa.momo_lib.utils.NumberUtils;

public class ClientController {

	public static final String BASE_DIR = "C:/gestion_banque/impressions";
	public static final String BASE_FILE_NAME = "fiche_client";
	public static final String EXT = "txt";

	private static ClientController instance = null;

	private ClientView clientView;
	private ClientDAO clientDAO;
	private AppView appView;
	private CompteBancaireView compteBancaireView;
	private CompteBancaireDAO compteBancaireDAO;

	private ClientController() {
		clientView = ClientView.getInstance();
		clientDAO = ClientDAO.getInstance();
		appView = AppView.getInstance();
		compteBancaireDAO = CompteBancaireDAO.getInstance();
		compteBancaireView = CompteBancaireView.getInstance();
	}

	public static ClientController getInstance() {

		if (instance == null)
			instance = new ClientController();

		return instance;
	}

	public void creation() {
		Client newClient = clientView.affichageCreation();
		newClient = clientDAO.save(newClient);
		appView.affichageMessage("Le client " + newClient.getId() + " est cr�e");
	}

	public void affichageAll() {
		List<Client> clients = clientDAO.findAll();
		clientView.affichageTous(clients);
	}

	public void suppression() {
		String idClient = clientView.affichageSuppression();
		if (!clientDAO.exists(idClient)) {
			appView.affichageMessage("Le client " + idClient + " n'existe pas");
			return;
		}

		List<CompteBancaire> comptesBancaires = compteBancaireDAO.findAllByIdClient(idClient);
		for (CompteBancaire currentCB : comptesBancaires) {
			compteBancaireDAO.deleteById(currentCB.getId());

		}
		clientDAO.deleteById(idClient);
		appView.affichageMessage("Le client " + idClient + " est supprime");
	}

	public void modification() {
		String idClient = clientView.recuperationIdClient();
		Client oldClient = clientDAO.findById(idClient);
		if (oldClient == null) {
			appView.affichageMessage("Le client " + idClient + " n'existe pas");
			return;
		}
		Client newClient = clientView.affichageModification(oldClient);
		clientDAO.update(newClient);
		appView.affichageMessage("Le client " + newClient.getId() + " est modifie");

	}

	public void chercherUnClient() {
		String choixCritere = clientView.recuperationCritereRecherche();
		List<Client> clients = new MyList<>();

		clients.add(clientDAO.findById(choixCritere));

		clients.addAll(clientDAO.findByNom(choixCritere));

		clients.add(compteBancaireDAO.findByNumeroCompte(NumberUtils.parseInt(choixCritere)));

		clientView.affichageTous(clients);

	}

	public void impressionInfosClient() {

		try {
			String idClient = clientView.recuperationInfosImpression();

			Client client = clientDAO.findById(idClient);

			if (client == null) {
				appView.affichageMessage("Le client " + idClient + " n'existe pas.");
				return;
			}

			List<CompteBancaire> compteBancaires = compteBancaireDAO.findAllByIdClient(idClient);
			if (compteBancaires == null || compteBancaires.size() == 0) {
				appView.affichageMessage("Le client " + idClient + " ne possede pas de comptes bancaires");
				return;
			}
			Date now = new Date();
			String fileName = BASE_FILE_NAME + "_" + idClient + "_" + now.getTime() + "." + EXT;
			Path cheminFile = Path.of(BASE_DIR, fileName);
			File file = cheminFile.toFile();

			if (!cheminFile.getParent().toFile().exists())
				cheminFile.getParent().toFile().mkdirs();
			if (!file.exists())
				file.createNewFile();
			PrintStream psMyFile = new PrintStream(file);
			compteBancaireView.impressionInfosClient(compteBancaires, psMyFile);
			psMyFile.close();
		} catch (IOException e) {
			appView.affichageMessage("Erreur technique lie au gestion des fichiers: " + e.getMessage());
		} catch (Exception e) {
			appView.affichageMessage("Erreur technique inconnue: " + e.getMessage());
		}

	}

}
