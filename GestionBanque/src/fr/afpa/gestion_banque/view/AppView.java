package fr.afpa.gestion_banque.view;

import java.util.Scanner;

public class AppView {
	public static Scanner scanner = new Scanner(System.in);

	private static AppView instance = null;

	private AppView() {
		super();
	}
	public static AppView getInstance() {

		if (instance == null)
			instance = new AppView();

		return instance;
	}

	public  String affichageMenu() {
		System.out.println("1-  Cr�er une agence");
		System.out.println("2-  Cr�er un client");
		System.out.println("3-  Cr�er un compte bancaire");
		System.out.println("4-  Modifier une agence");
		System.out.println("5-  Modifier un client");
		System.out.println("6-  Modifier un compte bancaire");
		System.out.println("7-  Supprimer une agence");
		System.out.println("8-  Supprimer un client");
		System.out.println("9-  Supprimer un compte bancaire");
		System.out.println("10- Afficher la liste de tous les agences");
		System.out.println("11- Afficher la liste de tous les clients");
		System.out.println("12- Afficher la liste de tous les comptes bancaires");
		System.out.println("13- Recherche de compte (num�ro de compte)");
		System.out.println("14- Recherche de client (Nom, Num�ro de compte, identifiant de client)");
		System.out.println("15- Afficher la liste des comptes d�un client (identifiant client)");
		System.out.println("16- Imprimer les infos client (identifiant client)");
		System.out.println("17- Quitter le programme");
		System.out.print("\n		Choix : ");

		return AppView.scanner.nextLine().trim();
	}

	public  void affichageMessage(String message) {
		System.out.println(message);
	}
}
