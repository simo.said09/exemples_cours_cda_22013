package fr.afpa.gestion_banque.view;

import java.util.List;

import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.momo_lib.utils.NumberUtils;

public class AgenceView {

	private static AgenceView instance = null;

	private AgenceView() {
		super();
	}

	public static AgenceView getInstance() {

		if (instance == null)
			instance = new AgenceView();

		return instance;
	}

	public Agence affichageCreation() {
		Agence newAgence = new Agence();
		System.out.println("Creation Agence : ");
		System.out.print("Nom : ");
		newAgence.setNom(AppView.scanner.nextLine().trim());
		System.out.print("Adresse : ");
		newAgence.setAdresse(AppView.scanner.nextLine().trim());

		return newAgence;
	}

	public void affichageToutes(List<Agence> agences) {
		System.out.println("Liste agences : ");
		System.out.printf("%-15s\t%-15s\t%s\t", "CODE_AGENCE", "NOM_AGENCE", "ADRESSE_AGENCE");
		System.out.print(System.lineSeparator());

		for (Agence currentAgence : agences) {
			System.out.printf(" %-14s\t %-14s\t %s\t", currentAgence.getCode(), currentAgence.getNom(),
					currentAgence.getAdresse());
			System.out.print(System.lineSeparator());
		}
	}

	public int affichageSuppression() {
		System.out.println("Suppression Agence : ");
		System.out.print("		Code agence : ");
		String codeAgence = AppView.scanner.nextLine().trim();
		return NumberUtils.parseInt(codeAgence);
	}

	public Agence affichageModification(Agence oldAgence) {

		System.out.print("Nom (" + oldAgence.getNom() + "): ");
		String newNomAgence = AppView.scanner.nextLine().trim();
		if (!newNomAgence.isBlank() && !newNomAgence.isEmpty())
			oldAgence.setNom(newNomAgence);
		System.out.print("Adresse (" + oldAgence.getAdresse() + "): ");
		String newAdresseAgence = AppView.scanner.nextLine().trim();
		if (!newAdresseAgence.isBlank() && !newAdresseAgence.isEmpty())
			oldAgence.setAdresse(newAdresseAgence);

		return oldAgence;
	}

	public int recuperationCodeAgence() {
		String codeAgence;
		System.out.println("Modification Agence : ");
		System.out.print("		Code agence : ");
		codeAgence = AppView.scanner.nextLine().trim();
		return NumberUtils.parseInt(codeAgence);
	}
}
