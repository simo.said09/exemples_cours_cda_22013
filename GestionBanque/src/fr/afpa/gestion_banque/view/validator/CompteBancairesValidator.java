package fr.afpa.gestion_banque.view.validator;

import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.gestion_banque.entities.CompteBancaire;

public class CompteBancairesValidator {

	private ClientDAO clientDAO;
	private AgenceDAO agenceDAO;
	private CompteBancaireDAO compteBancaireDAO;
	private static CompteBancairesValidator instance;

	public CompteBancairesValidator() {
		clientDAO = ClientDAO.getInstance();
		agenceDAO = AgenceDAO.getInstance();
		compteBancaireDAO = CompteBancaireDAO.getInstance();
	}

	public static CompteBancairesValidator getInstance() {

		if (instance == null)
			instance = new CompteBancairesValidator();

		return instance;
	}

	public boolean verificationRGs(CompteBancaire newCompteBancaire) {
		Client client = clientDAO.findById(newCompteBancaire.getClient().getId());
		if (client == null) {
			System.out.println("Le client " + newCompteBancaire.getClient().getId() + " n'existe pas");
			return false;
		}

		Agence agence = agenceDAO.findByCode(newCompteBancaire.getAgence().getCode());
		if (agence == null) {
			System.out.println("L'agence " + newCompteBancaire.getAgence().getCode() + " n'existe pas");
			return false;
		}

		if (!newCompteBancaire.getType().equalsIgnoreCase(CompteBancaire.TYPE_COMPTE_COURANT)
				&& !newCompteBancaire.getType().equalsIgnoreCase(CompteBancaire.TYPE_LIVRET_A)
				&& !newCompteBancaire.getType().equalsIgnoreCase(CompteBancaire.TYPE_PLAN_EPARGNE_LOGEMENT)) {
			System.out.println("Le type " + newCompteBancaire.getType() + " n'existe pas");
			return false;
		}

		if (compteBancaireDAO.aPlusQue3CompteComptes(newCompteBancaire.getClient().getId())) {
			System.out.println("Le client " + newCompteBancaire.getClient().getId() + " a deja 3 comptes");
			return false;
		}

		if (compteBancaireDAO.aUnCompteBancaireDuType(newCompteBancaire.getClient().getId(),
				newCompteBancaire.getType())) {
			System.out.println("Le client " + newCompteBancaire.getClient().getId() + " a deja un comptedu type <"
					+ newCompteBancaire.getType() + ">");
			return false;
		}

		return true;
	}
}
