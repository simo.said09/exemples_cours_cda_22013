package fr.afpa.gestion_banque.view;

import java.io.PrintStream;
import java.util.List;

import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.momo_lib.utils.DateUtils;
import fr.afpa.momo_lib.utils.NumberUtils;

public class CompteBancaireView {
	private static CompteBancaireView instance;

	private CompteBancaireView() {
		super();
	}

	public static CompteBancaireView getInstance() {

		if (instance == null)
			instance = new CompteBancaireView();

		return instance;
	}

	public CompteBancaire affichageCreation() {
		CompteBancaire newCompteBancaire = new CompteBancaire();
		System.out.println("Creation CompteBancaire : ");
		System.out.print("Solde : ");
		newCompteBancaire.setSolde(NumberUtils.parseDouble(AppView.scanner.nextLine().trim()));
		System.out.print("Decouvert : (O pour oui, autre pour non)");
		newCompteBancaire.setDecouvert("O".equalsIgnoreCase(AppView.scanner.nextLine().trim()));
		System.out.print("Type (CC, LA, PEL) : ");
		newCompteBancaire.setType(AppView.scanner.nextLine().trim());
		System.out.print("Id client : ");

		Client newClient = new Client();
		newClient.setId(AppView.scanner.nextLine().trim());
		newCompteBancaire.setClient(newClient);

		System.out.print("Code Agence : ");
		Agence newAgence = new Agence();
		newAgence.setCode(NumberUtils.parseInt(AppView.scanner.nextLine().trim()));
		newCompteBancaire.setAgence(newAgence);

		return newCompteBancaire;
	}

	public void affichageTous(List<CompteBancaire> compteBancaires) {

		System.out.println("Liste compteBancaires : ");
		System.out.printf("%-11s\t%-15s\t%-15s\t%-18s\t%-18s\t%s", "ID_COMPTE", "SOLDE", "DECOUVERT", "TYPE",
				"Nom Client", "CODE_AGENCE");
		System.out.print(System.lineSeparator());
		for (int i = 0; i < compteBancaires.size(); i++) {
			affichageBasique(compteBancaires.get(i));
		}

	}

	private void affichageBasique(CompteBancaire currentCompteBancaire) {
		System.out.printf("%011d\t%-15s\t%-15s\t%-18s\t%-18s\t%03d", currentCompteBancaire.getId(),
				currentCompteBancaire.getSolde(), currentCompteBancaire.isDecouvert(),
				currentCompteBancaire.getLibType(), currentCompteBancaire.getClient().getNom(),
				currentCompteBancaire.getAgence().getCode());
		System.out.print(System.lineSeparator());
	}

	public int affichageSuppression() {
		System.out.println("Suppression CompteBancaire : ");
		System.out.print("		Id compteBancaire : ");
		return NumberUtils.parseInt(AppView.scanner.nextLine().trim());
	}

	public CompteBancaire affichageModification(CompteBancaire oldCompteBancaire) {

		System.out.print("Solde (" + oldCompteBancaire.getSolde() + "): ");
		String newSoldeCompteBancaire = AppView.scanner.nextLine().trim();
		if (!newSoldeCompteBancaire.isBlank() && !newSoldeCompteBancaire.isEmpty())
			oldCompteBancaire.setSolde(NumberUtils.parseDouble(newSoldeCompteBancaire));

		System.out.print(
				"Decouvert (" + (oldCompteBancaire.isDecouvert() ? "Oui" : "Non") + " <O pour oui, autre pour non>): ");
		String newDecouvertCompteBancaire = AppView.scanner.nextLine().trim();
		if (!newDecouvertCompteBancaire.isBlank() && !newDecouvertCompteBancaire.isEmpty())
			oldCompteBancaire.setDecouvert(newDecouvertCompteBancaire.equalsIgnoreCase("o"));

		return oldCompteBancaire;
	}

	public int recuperationIdCompteBancaire(String message) {
		String idCompteBancaire;
		System.out.println(message);
		System.out.print("		ID compteBancaire : ");
		idCompteBancaire = AppView.scanner.nextLine().trim();
		return NumberUtils.parseInt(idCompteBancaire);
	}

	public void impressionInfosClient(List<CompteBancaire> compteBancaires, PrintStream myOut) {
		if (compteBancaires == null || compteBancaires.isEmpty())
			return;
		Client currentClient = compteBancaires.get(0).getClient();
		myOut.printf("%20s%10s%-30s%20s", "", "", "Fiche client", "");
		myOut.println();
		myOut.println("Numero client : " + currentClient.getId());
		myOut.println("Nom : " + currentClient.getNom());
		myOut.println("Prenom : " + currentClient.getPrenom());
		myOut.println("Date de naissance : " + DateUtils.format(currentClient.getDateNaissance(), "ddddd MMMMM yyyy"));
		myOut.println();
		myOut.println();
		myOut.println("_".repeat(80));
		myOut.println();
		myOut.printf("%20s%10s%-30s%20s", "", "", "Liste de compte", "");
		myOut.println();
		myOut.println("_".repeat(80));
		myOut.println();
		myOut.printf("%-28s%-25s", "Num�ro de compte", "Solde");
		myOut.println();
		myOut.println("_".repeat(80));
		myOut.println();
		String numero = null;
		String solde = null;
		String icone = null;

		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			numero = String.format("%011d", currentCompteBancaire.getId());
			solde = currentCompteBancaire.getSolde() + " euros";
			icone = currentCompteBancaire.getSolde() >= 0 ? ":-)" : ":-(";
			myOut.printf("%-28s%-25s%s", numero, solde, icone);
			myOut.println();
		}

	}

	public String recuperationIdClient(String message) {
		String idCompteBancaire;
		System.out.println(message);
		System.out.print("		ID client : ");
		idCompteBancaire = AppView.scanner.nextLine().trim();
		return idCompteBancaire;
	}

}
