package fr.afpa.gestion_banque.view;

import java.util.List;

import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.momo_lib.utils.DateUtils;

public class ClientView {
	
	private static ClientView instance = null;

	private ClientView() {
		super();
	}

	public static ClientView getInstance() {

		if (instance == null)
			instance = new ClientView();

		return instance;
	}

	public Client affichageCreation() {
		Client newClient = new Client();
		System.out.println("Creation Client : ");
		System.out.print("Nom : ");
		newClient.setNom(AppView.scanner.nextLine().trim());
		System.out.print("Prenom : ");
		newClient.setPrenom(AppView.scanner.nextLine().trim());
		System.out.print("Date naissance (" + DateUtils.getDefaultPattern() + "): ");

		newClient.setDateNaissance(DateUtils.parseDate(AppView.scanner.nextLine().trim()));
		System.out.print("Mail : ");
		newClient.setMail(AppView.scanner.nextLine().trim());

		return newClient;
	}

	public void affichageTous(List<Client> clients) {
		System.out.println("Liste clients : ");
		System.out.printf("%-8s\t%-15s\t%-15s\t%-18s\t%s", "ID", "NOM", "PRENOM", "DATE_NAISSANCE", "MAIL");
		System.out.print(System.lineSeparator());

		for (Client currentClient : clients) {
			System.out.printf("%-8s\t%-15s\t%-15s\t%-18s\t%s", currentClient.getId(), currentClient.getNom(),
					currentClient.getPrenom(), DateUtils.format(currentClient.getDateNaissance()),
					currentClient.getMail());
			System.out.print(System.lineSeparator());
		}
	}

	public String affichageSuppression() {
		System.out.println("Suppression Client : ");
		System.out.print("		Id client : ");
		String idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}

	public Client affichageModification(Client oldClient) {

		System.out.print("Nom (" + oldClient.getNom() + "): ");
		String newNomClient = AppView.scanner.nextLine().trim();
		if (!newNomClient.isBlank() && !newNomClient.isEmpty())
			oldClient.setNom(newNomClient);

		System.out.print("Prenom (" + oldClient.getPrenom() + "): ");
		String newPrenomClient = AppView.scanner.nextLine().trim();
		if (!newPrenomClient.isBlank() && !newPrenomClient.isEmpty())
			oldClient.setPrenom(newPrenomClient);

		System.out.print(
				"Date de naissance (" + oldClient.getDateNaissance() + " <" + DateUtils.getDefaultPattern() + "> ): ");
		String newDateClient = AppView.scanner.nextLine().trim();
		if (!newDateClient.isBlank() && !newDateClient.isEmpty())
			oldClient.setDateNaissance(DateUtils.parseDate(newDateClient));

		System.out.print("Mail (" + oldClient.getMail() + "): ");
		String newMailClient = AppView.scanner.nextLine().trim();
		if (!newMailClient.isBlank() && !newMailClient.isEmpty())
			oldClient.setMail(newMailClient);
		return oldClient;
	}

	public String recuperationIdClient() {
		String idClient;
		System.out.println("Modification Client : ");
		System.out.print("		ID client : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}

	public String recuperationCritereRecherche() {
		String idClient;
		System.out.println("Recherche de client (Nom, Num�ro de compte, identifiant de client) : ");
		System.out.print("		Choix : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}

	public String recuperationInfosImpression() {

		String idClient;
		System.out.println("Imprimer les infos client (identifiant client) : ");
		System.out.print("		ID client : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}
}
