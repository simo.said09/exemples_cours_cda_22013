package fr.afpa.gestion_banque.exceptions;

public class InitParametragesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1246743630258588584L;

	public InitParametragesException() {
		// TODO Auto-generated constructor stub
	}

	public InitParametragesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InitParametragesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InitParametragesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InitParametragesException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
