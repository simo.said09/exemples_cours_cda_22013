package fr.afpa.gestion_banque;

import fr.afpa.gestion_banque.controller.AgenceController;
import fr.afpa.gestion_banque.controller.AppController;
import fr.afpa.gestion_banque.controller.ClientController;
import fr.afpa.gestion_banque.controller.CompteBancaireController;
import fr.afpa.gestion_banque.view.AppView;

public class GestionBanqueApp {

	private AgenceController agenceController;
	private ClientController clientController;
	private AppController appController;
	private AppView appView;
	private CompteBancaireController compteBancaireController;

	public GestionBanqueApp() {
		agenceController = AgenceController.getInstance();
		clientController = ClientController.getInstance();
		appController = AppController.getInstance();
		appView = AppView.getInstance();
		compteBancaireController = CompteBancaireController.getInstance();
	}

	public void start() {

		do {
			appController.initData();
			
			String choixUtilisateur = appView.affichageMenu();

			if (choixUtilisateur.equalsIgnoreCase("1")) {
				agenceController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("2")) {
				clientController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("3")) {
				compteBancaireController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("4")) {
				agenceController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("5")) {
				clientController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("6")) {
				compteBancaireController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("7")) {
				agenceController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("8")) {
				clientController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("9")) {
				compteBancaireController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("10")) {
				agenceController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("11")) {
				clientController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("12")) {
				compteBancaireController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("13")) {
				compteBancaireController.chercherUnCompteBancaire();
			} else if (choixUtilisateur.equalsIgnoreCase("14")) {
				clientController.chercherUnClient();
			} else if (choixUtilisateur.equalsIgnoreCase("15")) {
				compteBancaireController.chercherTousCompteBancaireClient();
			} else if (choixUtilisateur.equalsIgnoreCase("16")) {

				clientController.impressionInfosClient();
			} else if (choixUtilisateur.equalsIgnoreCase("17")) {
				System.out.println("Au revoir");
				break;
			} else {
				System.out.println("Choix invalide");
			}
			System.out.println("Continuez ...");
			AppView.scanner.nextLine();
		} while (true);
	}

	public static void main(String[] args) {
		GestionBanqueApp app = new GestionBanqueApp();
		app.start();
	}

}
