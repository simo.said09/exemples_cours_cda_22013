package fr.afpa.gestion_banque.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.momo_lib.utils.MyList;

public class FileAgenceCsvService {

	public static final String FILE_NAME;
	private static FileAgenceCsvService instance;

	static {
		if (!ConfigApp.BASE_BDD_DIR.toFile().exists())
			ConfigApp.BASE_BDD_DIR.toFile().mkdirs();
		FILE_NAME = Agence.class.getSimpleName() + ".csv";
	}
	
	private FileAgenceCsvService() {
	}
	
	public static FileAgenceCsvService getInstance() {
		if(instance == null)
			instance = new FileAgenceCsvService();
		return instance;
	}
	public static void main(String[] args) throws IOException {
		FileAgenceCsvService fileAgenceCsvService = new FileAgenceCsvService();
		List<Agence> agences = fileAgenceCsvService.readAll();
		System.out.println(agences);
	}

	public void saveAll(List<Agence> agences) throws IOException {

		saveAll(agences, false);

	}

	public void saveAll(List<Agence> agences, boolean append) throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		for (Agence agence : agences) {
			pw.println(agence.toCsvLine());
			pw.flush();
		}

		pw.close();

	}

	public void save(Agence agence) throws IOException {

		save(agence, false);

	}

	public void save(Agence agence, boolean append) throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		pw.println(agence.toCsvLine());
		pw.flush();
		pw.close();

	}

	public List<Agence> readAll() throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileReader fw = new FileReader(filePath.toFile());
		BufferedReader pw = new BufferedReader(fw);
		String line;
		List<Agence> agences = new MyList<>();
		while (pw.ready()) {

			line = pw.readLine();
			if (!line.isBlank()) {
				agences.add(Agence.parseAgence(line));
			}
		}

		pw.close();
		return agences;
	}

}
