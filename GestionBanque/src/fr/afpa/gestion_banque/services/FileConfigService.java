package fr.afpa.gestion_banque.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.gestion_banque.exceptions.InitParametragesException;

public class FileConfigService {

	public static final String FILE_NAME = "app_config.properties";
	private static FileConfigService instance;

	public class Boite<KEY, VALUE> {
		private KEY key;
		private VALUE value;

		public Boite() {

		}

		public Boite(KEY key, VALUE value) {
			super();
			this.key = key;
			this.value = value;
		}

		public KEY getKey() {
			return key;
		}

		public void setKey(KEY key) {
			this.key = key;
		}

		public VALUE getValue() {
			return value;
		}

		public void setValue(VALUE value) {
			this.value = value;
		}

	}

	private FileConfigService() {
	}

	public static FileConfigService getInstance() {
		if (instance == null)
			instance = new FileConfigService();
		return instance;
	}

	public static void main(String[] args) throws IOException, InitParametragesException {
		FileConfigService fileConfigCsvService = new FileConfigService();

		Map<String, String> properties = fileConfigCsvService.readAll();
		for (Map.Entry<String, String> boite : properties.entrySet()) {
			System.out.println("Key = " + boite.getKey() + " , value = " + boite.getValue());
		}
	}

	public Map<String, String> readAll() throws InitParametragesException {

		try {
			Map<String, String> properties = new HashMap<String, String>();

			InputStream inStream = this.getClass().getClassLoader().getResourceAsStream("app_config.properties");
			InputStreamReader inReader = new InputStreamReader(inStream);
			BufferedReader br = new BufferedReader(inReader);
			String line;
			Boite<String, String> boite;
			while (br.ready()) {

				line = br.readLine();
				if (!line.isBlank()) {
					boite = parseProperty(line);
					if (boite != null) {
						properties.put(boite.getKey(), boite.getValue());
					}
				}
			}
			br.close();
			return properties;
		} catch (Exception e) {
			throw new InitParametragesException(e);
		}

	}

	public FileConfigService.Boite<String, String> parseProperty(String line) {
		if (line == null || line.isBlank())
			return null;
		String key = null;
		String value = null;
		String[] partsOfLine = line.split(ConfigApp.SEPARATOR_PROPERTIES);
		if (partsOfLine.length > 0) {
			key = !partsOfLine[0].isBlank() ? partsOfLine[0].trim() : null;
		}
		if (partsOfLine.length > 1) {
			value = !partsOfLine[1].isBlank() ? partsOfLine[1].trim() : null;
		}

		return new FileConfigService.Boite<String, String>(key, value);

	}

}
