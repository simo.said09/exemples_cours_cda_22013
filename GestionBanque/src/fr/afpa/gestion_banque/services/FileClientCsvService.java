package fr.afpa.gestion_banque.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.momo_lib.utils.MyList;

public class FileClientCsvService {

	public static final String FILE_NAME;
	private static FileClientCsvService instance;

	static {
		if (!ConfigApp.BASE_BDD_DIR.toFile().exists())
			ConfigApp.BASE_BDD_DIR.toFile().mkdirs();
		FILE_NAME = Client.class.getSimpleName() + ".csv";
	}

	private FileClientCsvService() {

	}

	public static FileClientCsvService getInstance() {
		if (instance == null)
			instance = new FileClientCsvService();
		return instance;
	}

	public void saveAll(List<Client> clients) throws IOException {

		saveAll(clients, false);

	}

	public void saveAll(List<Client> clients, boolean append) throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		for (Client client : clients) {
			pw.println(client.toCsvLine());
			pw.flush();
		}

		pw.close();

	}

	public void save(Client client) throws IOException {

		save(client, false);

	}

	public void save(Client client, boolean append) throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		pw.println(client.toCsvLine());
		pw.flush();
		pw.close();

	}

	public List<Client> readAll() throws IOException {

		Path filePath = Path.of(ConfigApp.BASE_BDD_DIR.toString(), FILE_NAME);
		FileReader fw = new FileReader(filePath.toFile());
		BufferedReader pw = new BufferedReader(fw);
		String line;
		List<Client> clients = new MyList<>();
		while (pw.ready()) {

			line = pw.readLine();
			if (!line.isBlank()) {
				clients.add(Client.parseClient(line));
			}
		}

		pw.close();
		return clients;
	}

}
