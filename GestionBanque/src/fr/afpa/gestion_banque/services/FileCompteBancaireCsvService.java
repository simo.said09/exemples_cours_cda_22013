package fr.afpa.gestion_banque.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;

import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.momo_lib.utils.MyList;

public class FileCompteBancaireCsvService {

	public static final String SEPARATOR = ";";
	public static final String FILE_NAME;
	public static final Path BASE_DIR = Path.of("C:/gestion_banque/BDD");
	private static FileCompteBancaireCsvService instance;

	static {
		if (!BASE_DIR.toFile().exists())
			BASE_DIR.toFile().mkdirs();
		FILE_NAME = CompteBancaire.class.getSimpleName() + ".csv";
	}
	private FileCompteBancaireCsvService() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) throws IOException {
		FileCompteBancaireCsvService fileCompteBancaireCsvService = new FileCompteBancaireCsvService();
		// List<CompteBancaire> compteBancaires = CompteBancaireDAO.generationStub(15);
		// fileCompteBancaireCsvService.saveAll(compteBancaires);
		List<CompteBancaire> compteBancaires = fileCompteBancaireCsvService.readAll();
		System.out.println(compteBancaires);
	}

	public static FileCompteBancaireCsvService getInstance() {
		if (instance == null)
			instance = new FileCompteBancaireCsvService();
		return instance;
	}
	
	public void saveAll(List<CompteBancaire> compteBancaires) throws IOException {

		saveAll(compteBancaires, false);

	}

	public void saveAll(List<CompteBancaire> compteBancaires, boolean append) throws IOException {

		Path filePath = Path.of(BASE_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		for (CompteBancaire compteBancaire : compteBancaires) {
			pw.println(compteBancaire.toCsvLine());
			pw.flush();
		}

		pw.close();

	}

	public void save(CompteBancaire compteBancaire) throws IOException {

		save(compteBancaire, false);

	}

	public void save(CompteBancaire compteBancaire, boolean append) throws IOException {

		Path filePath = Path.of(BASE_DIR.toString(), FILE_NAME);
		FileWriter fw = new FileWriter(filePath.toFile(), append);
		PrintWriter pw = new PrintWriter(fw);
		pw.println(compteBancaire.toCsvLine());
		pw.flush();
		pw.close();

	}

	public List<CompteBancaire> readAll() throws IOException {

		Path filePath = Path.of(BASE_DIR.toString(), FILE_NAME);
		FileReader fw = new FileReader(filePath.toFile());
		BufferedReader pw = new BufferedReader(fw);
		String line;
		List<CompteBancaire> compteBancaires = new MyList<>();
		while (pw.ready()) {

			line = pw.readLine();
			if (!line.isBlank()) {
				compteBancaires.add(CompteBancaire.parseCompteBancaire(line));
			}
		}

		pw.close();
		return compteBancaires;
	}

	

}
