package fr.afpa.gestion_banque.dao;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.gestion_banque.entities.CompteBancaire;
import fr.afpa.gestion_banque.services.FileCompteBancaireCsvService;
import fr.afpa.momo_lib.utils.MyList;

public class CompteBancaireDAO {

	private static CompteBancaireDAO instance;

	private static int cptCompteBancaire = 1;

	private FileCompteBancaireCsvService fileCompteBancaireCsvService;

	private ClientDAO clientDAO;
	private AgenceDAO agenceDAO;

	private CompteBancaireDAO() {
		fileCompteBancaireCsvService = FileCompteBancaireCsvService.getInstance();
		clientDAO = ClientDAO.getInstance();
		agenceDAO = AgenceDAO.getInstance();
	}

	public static CompteBancaireDAO getInstance() {

		if (instance == null)
			instance = new CompteBancaireDAO();

		return instance;
	}

	private static int nextId() {
		return cptCompteBancaire++;
	}

	public CompteBancaire save(CompteBancaire newCompteBancaire) {
		if (newCompteBancaire.getId() == null) {
			newCompteBancaire.setId(nextId());
		}
		try {
			fileCompteBancaireCsvService.save(newCompteBancaire, true);
		} catch (IOException e) {
			return null;
		}
		return newCompteBancaire;
	}

	public List<CompteBancaire> findAll() {
		return findAll(true);
	}

	public List<CompteBancaire> findAll(boolean lazy) {
		try {
			List<CompteBancaire>  compteBancaires = fileCompteBancaireCsvService.readAll();
			if (lazy)
				return compteBancaires;
			else {
				Client c = null;
				Agence a = null;
				
				for (CompteBancaire currentCompteBancaire : compteBancaires) {
					c= clientDAO.findById(currentCompteBancaire.getClient().getId());
					a= agenceDAO.findByCode(currentCompteBancaire.getAgence().getCode());
					currentCompteBancaire.setAgence(a);
					currentCompteBancaire.setClient(c);
				}
				
				return compteBancaires;
				
			}

		} catch (IOException e) {
			return new MyList<CompteBancaire>();
		}
	}
	public CompteBancaire findById(int id) {
		return findById(id,true);
	}
	public CompteBancaire findById(int id,boolean lazy) {
		List<CompteBancaire> compteBancaires = findAll(lazy);
		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire.getId() == id)
				return currentCompteBancaire;
		}
		return null;
	}

	public List<CompteBancaire> findAllByCodeAgence(int codeAgence) {
		List<CompteBancaire> compteBancaires = findAll();
		List<CompteBancaire> resultatsCompteBancaires = new MyList<CompteBancaire>();
		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire.getAgence().getCode() == codeAgence) {
				resultatsCompteBancaires.add(currentCompteBancaire);
			}
		}
		return resultatsCompteBancaires;
	}

	public List<CompteBancaire> findAllByIdClient(String idClient) {
		List<CompteBancaire> compteBancaires = findAll();
		List<CompteBancaire> resultatsCompteBancaires = new MyList<CompteBancaire>();
		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire.getClient().getId().equalsIgnoreCase(idClient)) {
				resultatsCompteBancaires.add(currentCompteBancaire);
			}
		}
		return resultatsCompteBancaires;
	}

	public int countClientCompteBancaire(String idClient) {
		List<CompteBancaire> compteBancaires = findAllByIdClient(idClient);
		return compteBancaires.size();
	}

	public boolean aPlusQue3CompteComptes(String idClient) {
		return countClientCompteBancaire(idClient) >= 3;
	}

	public boolean aUnCompteBancaireDuType(String idClient, String type) {
		List<CompteBancaire> compteBancaires = findAllByIdClient(idClient);
		for (CompteBancaire currentCompte : compteBancaires) {
			if (currentCompte.getType().equalsIgnoreCase(type))
				return true;
		}

		return false;
	}

	public static List<CompteBancaire> generationStub(int nombreClient) {
		List<CompteBancaire> compteBancaires = new MyList<>();
		String idClient;
		for (int i = 1; i <= nombreClient; i++) {
			idClient = "AB" + String.format("%06d", i);
			compteBancaires.addAll(plusieursCb(idClient));
		}
		return compteBancaires;
	}

	private static List<CompteBancaire> plusieursCb(String idClient) {
		List<CompteBancaire> comptesbancaires = new MyList<>();
		Random ran = new Random();
		int nombreComptesBancaires = ran.nextInt(3 + 1 - 1) + 1;

		if (nombreComptesBancaires >= 1) {
			comptesbancaires.add(unCB(idClient, CompteBancaire.TYPE_COMPTE_COURANT));
		}
		if (nombreComptesBancaires >= 2) {
			comptesbancaires.add(unCB(idClient, CompteBancaire.TYPE_LIVRET_A));
		}
		if (nombreComptesBancaires >= 3) {
			comptesbancaires.add(unCB(idClient, CompteBancaire.TYPE_PLAN_EPARGNE_LOGEMENT));
		}

		return comptesbancaires;
	}

	private static CompteBancaire unCB(String idClient, String typeCompte) {

		Random ran = new Random();
		int id = nextId();
		int codeAgence = ran.nextInt(10 + 1 - 1) + 1;
		CompteBancaire newCompteBancaire = new CompteBancaire();
		newCompteBancaire.setId(id);
		newCompteBancaire.setSolde(ran.nextDouble() * 1000);
		newCompteBancaire.setDecouvert(ran.nextBoolean());
		newCompteBancaire.setType(typeCompte);
		newCompteBancaire.setClient(ClientDAO.getInstance().findById(idClient));
		newCompteBancaire.setAgence(AgenceDAO.getInstance().findByCode(codeAgence));

		return newCompteBancaire;
	}

	public boolean exists(int id) {
		return findById(id) != null;
	}

	public boolean deleteById(int id) {
		List<CompteBancaire> compteBancairesTmp = findAll();
		int indexCompteBancaire = -1;
		for (int i = 0; i < compteBancairesTmp.size(); i++) {
			if (compteBancairesTmp.get(i).getId() == id) {
				indexCompteBancaire = i;
				break;
			}
		}
		if (indexCompteBancaire < 0) {
			return false;
		}
		compteBancairesTmp.remove(indexCompteBancaire);

		try {
			fileCompteBancaireCsvService.saveAll(compteBancairesTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean update(CompteBancaire newCompteBancaire) {
		List<CompteBancaire> compteBancairesTmp = findAll();
		int indexCompteBancaire = -1;
		for (int i = 0; i < compteBancairesTmp.size(); i++) {
			if (compteBancairesTmp.get(i).getId() == newCompteBancaire.getId()) {
				indexCompteBancaire = i;
				break;
			}
		}
		if (indexCompteBancaire < 0) {
			return false;
		}
		compteBancairesTmp.set(indexCompteBancaire, newCompteBancaire);

		try {
			fileCompteBancaireCsvService.saveAll(compteBancairesTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public Client findByNumeroCompte(Integer idCompte) {
		if (idCompte == null)
			return null;
		CompteBancaire compteBancaire = this.findById(idCompte,false);
		if (compteBancaire == null)
			return null;

		return compteBancaire.getClient();
	}
	public void initData() throws IOException {
		List<CompteBancaire> compteBancaires = CompteBancaireDAO.generationStub(7);
		fileCompteBancaireCsvService.saveAll(compteBancaires);
	}
}
