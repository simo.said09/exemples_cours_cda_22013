package fr.afpa.gestion_banque.dao;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import fr.afpa.gestion_banque.entities.Client;
import fr.afpa.gestion_banque.services.FileClientCsvService;
import fr.afpa.momo_lib.utils.MyList;

public class ClientDAO {

	private static int cptClient = 1;
	private static ClientDAO instance;

	private FileClientCsvService fileClientCsvService;

	private ClientDAO() {
		fileClientCsvService = FileClientCsvService.getInstance();
	}

	public static ClientDAO getInstance() {

		if (instance == null)
			instance = new ClientDAO();

		return instance;
	}

	private static String nextId() {
		String chaineFormatte = String.format("%06d", cptClient++);
		return "AB" + chaineFormatte;
	}

	public Client save(Client newClient) {
		if (newClient.getId() == null) {
			newClient.setId(nextId());
		}
		try {
			fileClientCsvService.save(newClient, true);
		} catch (IOException e) {
			return null;
		}
		return newClient;
	}

	public List<Client> findAll() {
		try {
			return fileClientCsvService.readAll();
		} catch (IOException e) {
			return new MyList<Client>();
		}
	}

	public Client findById(String id) {
		List<Client> clients = findAll();
		for (Client currentClient : clients) {
			if (currentClient.getId().equalsIgnoreCase(id))
				return currentClient;
		}
		return null;
	}

	public List<Client> findByNom(String nom) {
		List<Client> clients = findAll();
		List<Client> clientsResultats = new MyList<>();
		for (Client currentClient : clients) {
			if (currentClient.getNom().equalsIgnoreCase(nom))
				clientsResultats.add(currentClient);
		}
		return clientsResultats;
	}

	public boolean exists(String id) {
		return findById(id) != null;
	}

	public static List<Client> generationStub(int n) {
		List<Client> clients = new MyList<>();
		for (int i = 1; i <= n; i++) {
			clients.add(unClient(i));
		}
		return clients;
	}

	private static Client unClient(int i) {
		Random ran = new Random();
		Client currentClient = new Client();
		String id = nextId();
		currentClient.setId(id);
		String nomClient = "nom_client" + i;
		if (ran.nextBoolean())
			nomClient = "toto";
		currentClient.setNom(nomClient);
		currentClient.setPrenom("prenom_client" + i);
		currentClient.setDateNaissance(new Date());
		currentClient.setMail("nom_client" + i + "@gmail.com");
		return currentClient;
	}

	public boolean deleteById(String id) {
		List<Client> clientsTmp = findAll();
		int indexClient = -1;
		for (int i = 0; i < clientsTmp.size(); i++) {
			if (clientsTmp.get(i).getId().equalsIgnoreCase(id)) {
				indexClient = i;
				break;
			}
		}
		if (indexClient < 0) {
			return false;
		}
		clientsTmp.remove(indexClient);
		try {
			fileClientCsvService.saveAll(clientsTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean update(Client newClient) {
		List<Client> clientsTmp = findAll();
		int indexClient = -1;
		for (int i = 0; i < clientsTmp.size(); i++) {
			if (clientsTmp.get(i).getId().equalsIgnoreCase(newClient.getId())) {
				indexClient = i;
				break;
			}
		}
		if (indexClient < 0) {
			return false;
		}
		clientsTmp.set(indexClient,newClient);
		try {
			fileClientCsvService.saveAll(clientsTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void initData() throws IOException {
		List<Client> clients = ClientDAO.generationStub(15);
		fileClientCsvService.saveAll(clients);
	}

}
