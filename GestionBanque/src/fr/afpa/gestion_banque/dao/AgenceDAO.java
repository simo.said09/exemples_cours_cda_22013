package fr.afpa.gestion_banque.dao;

import java.io.IOException;
import java.util.List;

import fr.afpa.gestion_banque.entities.Agence;
import fr.afpa.gestion_banque.services.FileAgenceCsvService;
import fr.afpa.momo_lib.utils.MyList;

public class AgenceDAO {

	private static AgenceDAO instance;

	private static int cptAgence = 1;

	private FileAgenceCsvService fileAgenceCsvService;

	private AgenceDAO() {
		super();
		fileAgenceCsvService = FileAgenceCsvService.getInstance();
	}

	public static AgenceDAO getInstance() {

		if (instance == null)
			instance = new AgenceDAO();

		return instance;
	}

	private static int nextId() {
		return cptAgence++;
	}

	public Agence save(Agence newAgence) {
		if (newAgence.getCode() == null) {
			newAgence.setCode(nextId());
		}
		try {
			fileAgenceCsvService.save(newAgence, true);
		} catch (IOException e) {
			return null;
		}
		return newAgence;
	}

	public List<Agence> findAll() {
		try {
			return fileAgenceCsvService.readAll();
		} catch (IOException e) {
			return new MyList<>();
		}
	}

	public Agence findByCode(int codeAgence) {
		List<Agence> agences = findAll();
		for (Agence currentAgence : agences) {
			if (currentAgence.getCode() == codeAgence)
				return currentAgence;
		}
		return null;
	}

	public static List<Agence> generationStub(int n) {
		List<Agence> agences = new MyList<>();
		Integer id = null;

		agences.add(new Agence(0, "Agence generique", "INCONNUE"));

		for (int i = 2; i <= n; i++) {
			id = nextId();
			agences.add(new Agence(id, "agence" + i, "adresse" + i));
		}

		return agences;
	}

	public boolean exists(int code) {
		return findByCode(code) != null;
	}

	public boolean deleteByCode(int codeAgence) {
		List<Agence> agencesTmp = findAll();
		int indexAgence = -1;

		for (int i = 0; i < agencesTmp.size(); i++) {
			if (agencesTmp.get(i).getCode() == codeAgence) {
				indexAgence = i;
				break;
			}
		}

		if (indexAgence < 0) {
			return false;
		}

		agencesTmp.remove(indexAgence);
		try {
			fileAgenceCsvService.saveAll(agencesTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean update(Agence newAgence) {
		List<Agence> agencesTmp = findAll();
		int indexAgence = -1;
		for (int i = 0; i < agencesTmp.size(); i++) {
			if (agencesTmp.get(i).getCode() == newAgence.getCode()) {
				indexAgence = i;
				break;
			}
		}

		if (indexAgence < 0) {
			return false;
		}

		agencesTmp.set(indexAgence, newAgence);
		try {
			fileAgenceCsvService.saveAll(agencesTmp);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public void initData() throws IOException {
		List<Agence> agences = AgenceDAO.generationStub(11);
		fileAgenceCsvService.saveAll(agences);
	}
}
