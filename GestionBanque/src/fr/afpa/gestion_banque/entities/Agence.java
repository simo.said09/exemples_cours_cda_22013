package fr.afpa.gestion_banque.entities;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.momo_lib.utils.NumberUtils;

public class Agence {

	private Integer code;
	private String nom;
	private String adresse;
	// bi
	// private CompteBancaire[] compteBancaires;

	public Agence() {

	}
	public Agence(Integer code) {
		super();
		this.code = code;
	}
	public Agence(Integer code, String nom, String adresse) {
		super();
		this.code = code;
		this.nom = nom;
		this.adresse = adresse;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String toCsvLine() {
		String code = this.getCode() == null ? "" : this.getCode() + "";
		String adresse = this.getAdresse() == null ? "" : this.getAdresse();
		String nom = this.getNom() == null ? "" : this.getNom();
		return code + ConfigApp.SEPARATOR_CSV + nom + ConfigApp.SEPARATOR_CSV + adresse;
	}

	public static Agence parseAgence(String line) {
		if(line==null || line.isBlank())
			return null;
		
		
		Agence currentAgence = new Agence();
		String[] partsOfLine = line.split(ConfigApp.SEPARATOR_CSV);
		if(partsOfLine.length>0) {
			currentAgence.setCode(NumberUtils.parseInt(partsOfLine[0]));
		}
		if(partsOfLine.length>1) {
			currentAgence.setNom(!partsOfLine[1].isBlank()?partsOfLine[1]:null);
		}
		if(partsOfLine.length>2 ) {
			currentAgence.setAdresse(!partsOfLine[2].isBlank()?partsOfLine[2]:null);
		}
		return currentAgence;
	}

}
