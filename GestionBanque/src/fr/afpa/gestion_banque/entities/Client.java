package fr.afpa.gestion_banque.entities;

import java.util.Date;
import java.util.Objects;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.momo_lib.utils.DateUtils;

public class Client {

	private String id;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String mail;

	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Client(String id) {
		super();
		this.id = id;
	}

	public Client(String id, String nom, String prenom, Date dateNaissance, String mail) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance
				+ ", mail=" + mail + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateNaissance, id, mail, nom, prenom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		return Objects.equals(id, other.id);
	}

	public String toCsvLine() {
		return id + ConfigApp.SEPARATOR_CSV + nom + ConfigApp.SEPARATOR_CSV + prenom + ConfigApp.SEPARATOR_CSV
				+ DateUtils.format(dateNaissance) + ConfigApp.SEPARATOR_CSV + mail;
	}

	public static Client parseClient(String line) {
		if (line == null || line.isBlank())
			return null;
		Client currentClient = new Client();
		String[] partsOfLine = line.split(ConfigApp.SEPARATOR_CSV);
		String tmp;
		if (partsOfLine.length > 0) {
			tmp = (partsOfLine[0] != null && !partsOfLine[0].isBlank()) ? partsOfLine[0] : null;
			currentClient.setId(tmp);
		}
		if (partsOfLine.length > 1) {
			tmp = (partsOfLine[1] != null && !partsOfLine[1].isBlank()) ? partsOfLine[1] : null;
			currentClient.setNom(tmp);
		}
		if (partsOfLine.length > 2) {
			tmp = (partsOfLine[2] != null && !partsOfLine[2].isBlank()) ? partsOfLine[2] : null;
			currentClient.setPrenom(tmp);
		}
		if (partsOfLine.length > 3) {
			currentClient.setDateNaissance(DateUtils.parseDate(partsOfLine[3]));
		}
		if (partsOfLine.length > 4) {
			tmp = (partsOfLine[4] != null && !partsOfLine[4].isBlank()) ? partsOfLine[4] : null;
			currentClient.setMail(tmp);
		}
		return currentClient;
	}
}
