package fr.afpa.gestion_banque.entities;

import fr.afpa.gestion_banque.config.ConfigApp;
import fr.afpa.momo_lib.utils.NumberUtils;

public class CompteBancaire {

	public static final String TYPE_COMPTE_COURANT = "CC";
	public static final String TYPE_LIVRET_A = "LA";
	public static final String TYPE_PLAN_EPARGNE_LOGEMENT = "PEL";

	private Integer id;
	private Double solde;
	private String type;
	private Boolean decouvert;
	private Agence agence;
	private Client client;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getSolde() {
		return solde;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(Boolean decouvert) {
		this.decouvert = decouvert;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getLibType() {

		if ("CC".equalsIgnoreCase(this.type))
			return "Compte courant";
		if ("LA".equalsIgnoreCase(this.type))
			return "Livret A";
		if ("PEL".equalsIgnoreCase(this.type))
			return "Plan epargne logement";

		return null;
	}

	public String toCsvLine() {
		return id + ConfigApp.SEPARATOR_CSV + solde + ConfigApp.SEPARATOR_CSV + type + ConfigApp.SEPARATOR_CSV + decouvert
				+ ConfigApp.SEPARATOR_CSV + agence.getCode() + ConfigApp.SEPARATOR_CSV + client.getId();
	}

	public static CompteBancaire parseCompteBancaire(String line) {
		if (line == null || line.isBlank())
			return null;
		CompteBancaire currentCompteBancaire = new CompteBancaire();
		String[] partsOfLine = line.split(ConfigApp.SEPARATOR_CSV);
		String tmp;
		if (partsOfLine.length > 0) {
			tmp = (partsOfLine[0] != null && !partsOfLine[0].isBlank()) ? partsOfLine[0] : null;
			currentCompteBancaire.setId(NumberUtils.parseInt(partsOfLine[0]));
		}
		if (partsOfLine.length > 1) {
			currentCompteBancaire.setSolde(NumberUtils.parseDouble(partsOfLine[1]));
		}
		if (partsOfLine.length > 2) {
			tmp = (partsOfLine[2] != null && !partsOfLine[2].isBlank()) ? partsOfLine[2] : null;
			currentCompteBancaire.setType(tmp);
		}
		if (partsOfLine.length > 3) {
			currentCompteBancaire.setDecouvert(NumberUtils.parseBoolean(partsOfLine[3]));
		}
		if (partsOfLine.length > 4) {
			Agence agence = null;
			Integer codeAgence = NumberUtils.parseInt(partsOfLine[4]);
			if (codeAgence != null)
				agence = new Agence(codeAgence);

			currentCompteBancaire.setAgence(agence);
		}
		if (partsOfLine.length > 5) {
			Client client = null;
			tmp = (partsOfLine[5] != null && !partsOfLine[5].isBlank()) ? partsOfLine[5] : null;
			if (tmp != null)
				client = new Client(tmp);
			
			currentCompteBancaire.setClient(client);
		}
		return currentCompteBancaire;
	}
}
