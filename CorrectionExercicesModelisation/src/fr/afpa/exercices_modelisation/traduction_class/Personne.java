package fr.afpa.exercices_modelisation.traduction_class;

public class Personne {
	private int id;
	private String nom;
	private String prenom;
	// ManyToOne
	private Personne parent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Personne getParent() {
		return parent;
	}

	public void setParent(Personne parent) {
		this.parent = parent;
	}

}
