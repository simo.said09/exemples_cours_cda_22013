package fr.afpa.exercices_modelisation.traduction_class.manytomany_non_p;

public class Personne {
	private int id;
	private String nom;
	private String prenom;
	// ManyToMany
	private Personne[] parents;
	// ManyToMany sens inverse
	//private PersonneBis[] enfants;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Personne[] getParents() {
		return parents;
	}

	public void setParents(Personne[] parents) {
		this.parents = parents;
	}

}
