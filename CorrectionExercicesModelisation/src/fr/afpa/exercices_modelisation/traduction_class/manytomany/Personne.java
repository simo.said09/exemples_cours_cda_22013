package fr.afpa.exercices_modelisation.traduction_class.manytomany;

public class Personne {
	private int id;
	private String nom;
	private String prenom;
	
	// ManyToMany ()
	private Lien[] liensParents;
	private Lien[] liensEnfants;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Lien[] getLiensParents() {
		return liensParents;
	}

	public void setLiensParents(Lien[] liensParents) {
		this.liensParents = liensParents;
	}

	public Lien[] getLiensEnfants() {
		return liensEnfants;
	}

	public void setLiensEnfants(Lien[] liensEnfants) {
		this.liensEnfants = liensEnfants;
	}



}
