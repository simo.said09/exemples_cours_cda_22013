package fr.afpa.exercices_modelisation.traduction_class.manytomany;

public class Lien {

	private Personne parent;
	private Personne enfant;
	private String champ;

	public Personne getParent() {
		return parent;
	}

	public void setParent(Personne parent) {
		this.parent = parent;
	}

	public Personne getEnfant() {
		return enfant;
	}

	public void setEnfant(Personne enfant) {
		this.enfant = enfant;
	}

	public String getChamp() {
		return champ;
	}

	public void setChamp(String champ) {
		this.champ = champ;
	}

}
