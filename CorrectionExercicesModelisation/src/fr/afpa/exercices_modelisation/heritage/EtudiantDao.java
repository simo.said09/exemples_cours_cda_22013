package fr.afpa.exercices_modelisation.heritage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EtudiantDao implements Dao<Etudiant>{

	public EtudiantDao() {
		// TODO Auto-generated constructor stub
	}



	@Override
	public List<Etudiant> findAll() {
		List<Etudiant> resultat = new ArrayList<>();
		resultat.add(new Etudiant("E1", "toto1", "tata1", new Date(), "", "ABL0001"));
		resultat.add(new Etudiant("E2", "toto2", "tata2", new Date(), "", "ABL0002"));
		resultat.add(new Etudiant("E3", "toto3", "tata3", new Date(), "", "ABL0003"));
		return resultat;

	}



	@Override
	public Etudiant save(Etudiant t) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public boolean delete(int d) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public Etudiant findById(int d) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public boolean update(Etudiant t) {
		// TODO Auto-generated method stub
		return false;
	}



}
