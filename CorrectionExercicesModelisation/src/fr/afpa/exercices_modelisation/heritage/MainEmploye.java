package fr.afpa.exercices_modelisation.heritage;

import java.util.List;

public class MainEmploye {

	public MainEmploye() {

	}

	public static void main(String[] args) {
		/*
		 * Employe e = new Employe("123", "toto", "tata", new Date(), "", 30000D);
		 * System.out.println(e); System.out.println(e.calculePrime(3));
		 */

		/*
		 * Employe eCadre = new EmployeCadre("123", "toto", "tata", new Date(), "",
		 * 30000D,"EmployeCadre"); System.out.println(eCadre);
		 * System.out.println(eCadre.calculePrime(3));
		 * 
		 * Employe eNonCadre = new EmployeNonCadre("123", "toto", "tata", new Date(),
		 * "", 30000D,"EmployeNonCadre"); System.out.println(eNonCadre);
		 * System.out.println(eNonCadre.calculePrime(3));
		 */

		ClientDao clientDao = new ClientDao();

		EtudiantDao etudiantDao = new EtudiantDao();

		faireTrtBDD(clientDao);
		faireTrtBDD(etudiantDao);
		
		//Dao<Client> rrr = new Dao<>();

	}


	public static void faireTrtBDD(Dao<?> dao) {

		List<?> list = dao.findAll();
		for (Object object : list) {
			System.out.println(object);
		}

	}

}
