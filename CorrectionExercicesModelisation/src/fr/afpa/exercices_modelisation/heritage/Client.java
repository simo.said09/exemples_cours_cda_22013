package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class Client extends Personne{

	private String numeroCompte = "12";;
	
	public Client() {
		// TODO Auto-generated constructor stub
	}
	public Client(String id, String nom, String prenom, Date dateNaissance, String mail, String numeroCompte) {
		super(id, nom, prenom, dateNaissance, mail);
		this.numeroCompte = numeroCompte;
		}
	public String getNumeroCompte() {
		return numeroCompte;
	}

	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}

	@Override
	public String toString() {
		return "Client [id=" + this.getId() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom()
				+ ", dateNaissance=" + this.getDateNaissance() + ", mail=" + this.getMail() + ", numeroCompte="
				+ numeroCompte + "]";
	}

}
