package fr.afpa.exercices_modelisation.heritage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientDao implements Dao<Client> {

	public ClientDao() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Client save(Client t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int d) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Client findById(int d) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Client t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Client> findAll() {
		List<Client> resultat = new ArrayList<Client>();
		resultat.add(new Client("1", "toto1", "tata1", new Date(), "", "CL0001"));
		resultat.add(new Client("2", "toto2", "tata2", new Date(), "", "CL0002"));
		resultat.add(new Client("3", "toto3", "tata3", new Date(), "", "CL0003"));
		return resultat;

	}

}
