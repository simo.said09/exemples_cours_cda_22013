package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class Personne {
	private String id = "12";
	private String nom = "12";
	private String prenom = "12";
	private Date dateNaissance = new Date();
	private String mail = "12";

	public Personne() {
		super();
	}

	public Personne(String id, String nom, String prenom, Date dateNaissance, String mail) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.mail = mail;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void trt() {
		System.out.println("je suis le TRT de la classe Personne");
	}

}
