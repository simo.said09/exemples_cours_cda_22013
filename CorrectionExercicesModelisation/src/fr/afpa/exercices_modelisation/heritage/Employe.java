package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public abstract class Employe extends Personne implements Comparable<Employe>,Cloneable {

	private Double salaire;

	public Employe() {
	}

	public Employe(String id, String nom, String prenom, Date dateNaissance, String mail, Double salaire) {
		super(id, nom, prenom, dateNaissance, mail);
		this.salaire = salaire;
	}

	public Double getSalaire() {
		return salaire;
	}

	public void setSalaire(Double salaire) {
		this.salaire = salaire;
	}

	public abstract Double calculePrime(int pourcentage);

	@Override
	public String toString() {
		return "Employe [id=" + this.getId() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom() + ", dateNaissance=" + this.getDateNaissance()
		+ ", mail=" + this.getMail() + ", salaire=" + salaire + "]";
	}

}
