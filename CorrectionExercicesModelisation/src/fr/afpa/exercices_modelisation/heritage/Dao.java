package fr.afpa.exercices_modelisation.heritage;

import java.util.List;

public interface Dao<T> {

	public T save(T t );
	public boolean delete(int d);
	public T findById(int d);
	public boolean update(T t);
	public List<T> findAll();
	
}
