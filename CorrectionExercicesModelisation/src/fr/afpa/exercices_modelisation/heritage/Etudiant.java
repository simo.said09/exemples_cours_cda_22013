package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class Etudiant extends Personne {

	private String numeroCarte = "12";

	public Etudiant() {
		System.out.println(numeroCarte);
	}

	/*
	 * public Etudiant(String id, String nom, String prenom, Date dateNaissance,
	 * String mail,String numeroCarte) {
	 * 
	 * this.numeroCarte = numeroCarte; this.setId(id); this.setNom(nom);
	 * this.setPrenom(prenom); this.setDateNaissance(dateNaissance);
	 * this.setMail(mail); }
	 */
	public Etudiant(String id, String nom, String prenom, Date dateNaissance, String mail, String numeroCarte) {
		super(id, nom, prenom, dateNaissance, mail);
		this.numeroCarte = numeroCarte;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numeroCarte = numeroCarte;
	}

	public void blabla() {
		// this.trt();
		super.trt();
	}

	@Override
	public void trt() {
		// super.trt();
		System.out.println("je suis le TRT de la classe Etudiant");
	}
	
	@Override
	public String toString() {
		return "Etudiant [id=" + this.getId() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom() + ", dateNaissance=" + this.getDateNaissance()
		+ ", mail=" + this.getMail() + ", numeroCarte=" + numeroCarte + "]";
	}

}
