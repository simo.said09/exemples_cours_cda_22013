package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class EmployeNonCadre extends Employe{

	private String champSpecialeNonCadre;
	
	public EmployeNonCadre() {
	}

	public EmployeNonCadre(String id, String nom, String prenom, Date dateNaissance, String mail, Double salaire,String champSpecialeNonCadre) {
		super(id, nom, prenom, dateNaissance, mail,salaire);
		this.champSpecialeNonCadre = champSpecialeNonCadre;
	}

	public String getChampSpecialeNonCadre() {
		return champSpecialeNonCadre;
	}

	public void setChampSpecialeNonCadre(String champSpecialeNonCadre) {
		this.champSpecialeNonCadre = champSpecialeNonCadre;
	}
	
	@Override
	public Double calculePrime(int pourcentage) {
		return this.getSalaire() * 0.58D * ((double)pourcentage/100);
	}
	
	@Override
	public String toString() {
		return "Employe [id=" + this.getId() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom() + ", dateNaissance=" + this.getDateNaissance()
		+ ", mail=" + this.getMail() + ", salaire=" + this.getSalaire() +", champSpecialeNonCadre=" + this.getChampSpecialeNonCadre() + "]";
	}

	@Override
	public int compareTo(Employe o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
