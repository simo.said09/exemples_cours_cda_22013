package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class Main {

	public static void main(String[] args) {

		Etudiant edd = new Etudiant("123", "toto", "tata", new Date(), "", "CARTE125386T8U3ET68");
		edd.blabla();

		Personne p = new Personne();
		p = new Etudiant();
		Object o = new Object();
		o = new Personne();
		o = new Etudiant();

		if (o instanceof Client) {
			Client tClient = (Client) o;
		}
		if (o instanceof Personne) {
			Personne tClient = (Etudiant) o;
		}
		if (Personne.class.isAssignableFrom(o.getClass()) ) {
			Personne tClient = (Etudiant) o;
		}
		if (o.getClass().equals(Personne.class)) {
			Personne tClient = (Etudiant) o;
		}
		// tEtudiant.getId();

		Etudiant e = new Etudiant();
		// e= new Personne();

	}

}
