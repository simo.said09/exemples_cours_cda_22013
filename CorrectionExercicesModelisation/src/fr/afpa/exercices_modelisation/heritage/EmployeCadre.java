package fr.afpa.exercices_modelisation.heritage;

import java.util.Date;

public class EmployeCadre extends Employe {

	private String champSpecialeCadre;

	public EmployeCadre() {

	}

	public EmployeCadre(String id, String nom, String prenom, Date dateNaissance, String mail, Double salaire,
			String champSpecialeCadre) {
		super(id, nom, prenom, dateNaissance, mail, salaire);
		this.champSpecialeCadre = champSpecialeCadre;
	}

	public String getChampSpecialeCadre() {
		return champSpecialeCadre;
	}

	public void setChampSpecialeCadre(String champSpecialeCadre) {
		this.champSpecialeCadre = champSpecialeCadre;
	}
	
	@Override
	public String toString() {
		return "Employe [id=" + this.getId() + ", nom=" + this.getNom() + ", prenom=" + this.getPrenom()
				+ ", dateNaissance=" + this.getDateNaissance() + ", mail=" + this.getMail() + ", salaire="
				+ this.getSalaire() + ", champSpecialeCadre=" + this.getChampSpecialeCadre() + "]";
	}

	@Override
	public Double calculePrime(int pourcentage) {
		return this.getSalaire() * 0.55D * ((double)pourcentage/100);
	}

	@Override
	public int compareTo(Employe o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
